"use strict";

const program = require('commander');
const VPC = require("./lib/ec2/vpc");
const Subnet = require("./lib/ec2/subnet");
const allSubnets = require("./lib/runners/all-subnets");
const IP = require("./lib/ec2/IP");
const SG = require("./lib/ec2/security-group");
const runners = require("./lib/runners");

const options = {
    accessKeyId: 'AKIAJYA7PBBNCMAKVMDA',
    secretAccessKey: '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',
    region: 'us-east-2',
    log: console.log,
    DryRun: false
};

program.version('0.0.1');
 
program.command('vpc')
    .option("-l, --list", "List all VPCs")
    .action(opts => {
        if (opts.list) {
            VPC.GetAll(options)
            .then(vpcs => vpcs.forEach(vpc => vpc.print()))
            .catch(err => console.log(err));
        }
    });

program.command('subnet')
    .option("-l, --list", "List all Subnets")
    .action(opts => {
        if (opts.list) {
            allSubnets()
            .then(subnets => subnets.forEach(subnet => subnet.print()))
            .catch(err => console.log(err));
        }
    });

program.command('stack')
    .option("-l, --list", "List all Stacks")
    .option("-c, --create", "Create new stack")
    .option("-d, --deleteAll", "Create new stack")
    .action(opts => {
        const Stack = require("./lib/ops-works/stack");

        if (opts.create) {
            const config = require("./configurations/ops-works");

            runners.opsWorks(options, config)
            .then(data => data.print())
            .catch(err =>  console.log(err))

            //  allSubnets()
            // .then(subnets => {
            //     const subnet = subnets[0];
            //     const stack = new Stack(options);

            //     const stackOptions = { 
            //         subnet: subnet,
            //         name: "ttrr",
            //         region: "us-west-2",
            //         cookbooksSource: {
            //             type: "s3",
            //             url: "https://s3-us-west-2.amazonaws.com/cf-templates-12wj65pvo1zx9-us-west-2/hello-chef-node/nodejs.tar.gz"
            //         },
            //         sshKey: "test-ec2"
            //     };

            //     stack.create(stackOptions)
            //     .then(data => console.log(data))
            //     .catch(err =>console.log(err));
        //    })
        //    .catch(err => console.log(err)); 
        } else if (opts.list) {
             Stack.GetAll(options)
            .then(stacks => stacks.forEach(stack => stack.print()))
            .catch(err => console.log(err));
        } else if (opts.deleteAll) {
             Stack.GetAll(options)
            .then(stacks => {
                stacks.forEach(stack => {
                    stack.print()
                    stack.delete()
                });
            })
        }
    });

program.command('sg')
    .option("-l, --list", "List all Security Groups")
    .option("-c, --create", "Create new security group")
    .action(opts => {
        const ENV = "DEVQA";
        const FUNC = "ALL";

        if (opts.create) {
            VPC.GetAll(options)
            .then(vpcs => {
                const vpc = vpcs[0];
                vpc.print();
                const sg = new SG(options);
                
                sg.create({
                    description: "Test Security Group",
                    name: "MySG",
                    vpcId: vpc.id,
                    ENV: "DEVQA",
                    FUNC: "ALL",
                    ingress: [{
                        fromPort: "22",
                        cidrIp: "0.0.0.0/0",
                        toPort: "22",
                        ipProtocol: "tcp"
                    }]
                })
                .then(data => data.print())
                .catch(err => console.log(err));
            })
            .catch(err => console.log(err));

        } else if (opts.list) {
            options.groupName = "MySG";
             SG.GetAll(options)
            .then(groups => groups.forEach(group => group.print()))
            .catch(err => console.log(err));
        }

    });
 
program.parse(process.argv);

module.exports = { };