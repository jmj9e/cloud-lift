const Base = require('./elb-base');
const AWS = require('aws-sdk');

const ELB = class extends Base {

    constructor (options, data) {
        super(options);

        if (data) {
            this.id = data.AllocationId;
            this.data = data;
        }
    };

    create (options) {

        const fn = (resolve, reject) => {

        };

        return new Promise(fn);
    };

    delete (options) {

        const fn = (resolve, reject) => {

        };

        return new Promise(fn);
    };

    update (options) {

        const fn = (resolve, reject) => {

        };

        return new Promise(fn);
    };
};

return ELB;
