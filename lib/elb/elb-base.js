"use strict";

const LiftBase = require('../lift-base');

const Base = class extends LiftBase {
    constructor (options) {
        super(options, "ELB");
    };

    tag (name, value) {
        const params = this.params;

        params.Resources = [this.id];
        params.Tags = params.Tags || [];
        params.Tags.push({ Key: name, Value: value });

        const fn = (resolve, reject) => {
            this.service.createTags(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        };

        return new Promise(fn);
    };

    static set resourceName (name) {
        this._resourceName = name;
    };

    static NameTag (resourceName, env, func) {
        return `${resourceName}-${env}-${func}`;
    };
};

module.exports = Base;
