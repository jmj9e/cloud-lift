var AWS = require('aws-sdk');
var uuid = require('node-uuid');

// Create an S3 client
const s3 = new AWS.S3({apiVersion: '2006-03-01'});

// Create a bucket and upload something into it
var bucketName = 'node-sdk-sample-' + uuid.v4();
var keyName = 'hello_world.txt';

// s3.createBucket({ Bucket: bucketName }, function(err, data) {
//   var params = { Bucket: bucketName, Key: keyName, Body: 'Hello World!' };
//   // s3.putObject(params, function(err, data) {
//   //   if (err)
//   //     console.log(err)
//   //   else
//   //     console.log("Successfully uploaded data to " + bucketName + "/" + keyName);
//   // });
// });

// s3.listObjects()

// s3.listBuckets(function(err, data) {
//   if (err) console.log(err, err.stack); // an error occurred
//   else {  
//   	console.log(data); 
//   	data.Buckets.forEach(b => {
//   		const params = {
//   			Bucket: b.Name
//   		};
//   		s3.deleteBucket(params, function (err, data) {
// 	  		if (err) {
// 	  			s3.listObjects(params, function (err, data){
// 	  				console.log(data)
// 	  			})
// 	  		}
//   		});

//   	})  
//   }
// });


const promisify = require("./lib/promisify")(s3);

// const promisify = function (fn, params) {
//     return new Promise(function(resolve, reject) {
//         if (params) {
//             s3[fn](params, function(err, data) {
//                 err ? reject(err) : resolve(data);
//             });
//         } else {
//             s3[fn](function(err, data) {
//                 err ? reject(err) : resolve(data);
//             });
//         }
//     });
// };

const createBucket = (name) => {
    const params = {
        Bucket: name
    };

    return promisify("createBucket", params);
};

const deleteBucket = (bucket) => {
    const params = {
        Bucket: bucket.Name
    };

    return promisify("deleteBucket", params);
};
const listBuckets = () => promisify("listBuckets");

const listObjects = (bucket) => {
     const params = {
        Bucket: bucket.Name
    };

    return promisify("listObjects", params)
};

// createBucket(bucketName).then(data => {
//     console.log(data)
//     listBuckets().then((b)=> {
//         console.log(b)

//         b.forEach(a => deleteBucket(a.Name));
//     })
// })
// .catch(err => console.log(err))

 //   console.log(data)
listBuckets().then((b)=> {
    console.log(b)

    b.Buckets.forEach(bucket => deleteBucket(bucket));
});