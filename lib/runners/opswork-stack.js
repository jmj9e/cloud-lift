"use strict";

const allSubnets = require("./all-subnets");
const Layer = require("../ops-works/layer");
const IP = require("../ec2/IP");
const SG = require("../ec2/security-group");
const Stack = require("../ops-works/stack");
const Instance = require("../ops-works/instance");

module.exports = (options, config) => {
    const fn = (resolve, reject) => {
        allSubnets(options)
        .then(subnets => {
            const subnet = subnets[0];
            config.stack.subnetId = subnet.id;
            config.stack.vpcId = subnet.vpcId;
            
            return subnet;
        })
        .then(subnet => Stack.GetAll(options))
        .then(stacks => {
            const match = stacks.find(stack => stack.name === config.stack.name);
            return match ? match.update(config.stack) : new Stack(options).create(config.stack);
        })
        .then(stack => {
            config.stack.id = stack.id;
            options.groupNames = config.securityGroups.map(group => group.name);
         
            return SG.GetAll(options)
        })
        .then(secGroups => {
            const p = config.securityGroups.map(sg => {
                const match = secGroups.find(elem => elem.name +"     "+ sg.name);
            
                if (match) {
                    return match.update(sg);
                } else {
                    sg.vpcId = config.stack.vpcId;
                    return new SG(options).create(sg);
                }
            });

            return Promise.all(p);
        })
        .then(() => SG.GetAll(options))
        .then(securityGroups => {
            config.layers.forEach(layer => {
                const matches = layer.securityGroupNames.map(sgName => {   
                    return securityGroups.find(elem => elem.name === sgName);
                });

                if (matches) {
                    layer.securityGroupIds = matches.map(match => match.id);
                } else {
                    console.log("No Subnet Id found!")
                }  
            });
        })
        .then(() => Layer.GetAll(options, config.stack.id))
        .then(layers => {
            const p = config.layers.map(layer => {
                layer.stackId = config.stack.id;
                const match = layers.find(elem => elem.name === layer.name);
  
                return match ? match.update(layer) : new Layer(options).create(layer);
            });

            return Promise.all(p);
        })
        .then(layers => {
            const p = layers.map(layer => {
                const layerConfig = config.layers.find(elem => elem.name === layer.name);

                // create insances
                const instance = new Instance(options);

                const params = {
                    type: layerConfig.instance.type,
                    stackId: config.stack.id,
                    layerIds: [ layer.id ],
                    name: instance.name,
                    sshKey: layerConfig.sshKeyName,
                    subneyId: config.stack.subnetId
                };

                return instance.create(params);
            });

            return Promise.all(p);
        })
        .then(instances => {
            console.log(instances);
        })
        .catch(err => reject(err));
    };

    return new Promise(fn);
};
