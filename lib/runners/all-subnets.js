"use strict";

const VPC = require("../ec2/vpc");
const Subnet = require("../ec2/subnet");

const fn = options => {
    const fn = (resolve, reject) => {
        VPC.GetAll(options)
        .then(vpcs => vpcs.map(vpc => vpc.id))
        .then(vpcIds => {
            options.vpcs = vpcIds;
            return Subnet.GetAll(options);
        })
        .then(subnets => {
            if (options.print) {
                subnets.forEach(subnet => subnet.print());
            }

            resolve(subnets);
        })
        .catch(err => {
            if (options.print) {
                console.log(err)
            }

            reject(err)
        });
    };

    return new Promise(fn);
};

module.exports = fn;
