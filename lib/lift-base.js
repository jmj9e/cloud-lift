"use strict"

const AWS = require('aws-sdk');
const prettyjson = require('prettyjson');

AWS.config.apiVersions = {
    ec2: '2016-11-15',
    iam: '2010-05-08',
    opsworks: '2013-02-18',
    elb: '2012-06-01'
};

const LiftBase = class {
    constructor (options, serviceName) {
        this.service = new AWS[serviceName](options);
        this.log = console.log;
        this.debug = console.log;

        this._dry = options.dryRun || false;
        this._options = options;
    };

    get id () {
        return this._id;
    };

    set id (id) {
        this._id = id;
    };

    get vpcId () {
        return this._vpcId;
    }

    set vpcId (id) {
        this._vpcId = id;
    }

    get data () {
        return this._data;
    };

    set data (data) {
        this._data = data;
    };

    set dryRun (flag) {
        this._dry = flag;
    };

    get dryRun () {
        return this._dry;
    };

    get params () {
        return {
            DryRun: this.dryRun
        };
    };

    set name (name) {
        return this._name = name;
    };

    get name () {
        return this._name;
    };

    print () {
        this.log("----------------------------------------------------------");
        this.log(prettyjson.render(this._data));
        this.log("----------------------------------------------------------");
    };

};

module.exports = LiftBase;

// instanceExists, 
// bundleTaskComplete, 
// conversionTaskCancelled, 
// conversionTaskCompleted, 
// conversionTaskDeleted, 
// customerGatewayAvailable, 
// exportTaskCancelled, 
// exportTaskCompleted, 
// imageExists, 
// imageAvailable, 
// instanceRunning, 
// instanceStatusOk, 
// instanceStopped, 
// instanceTerminated, 
// keyPairExists, 
// natGatewayAvailable, 
// networkInterfaceAvailable, 
// passwordDataAvailable, 
// snapshotCompleted, 
// spotInstanceRequestFulfilled, 
// subnetAvailable, systemStatusOk, 
// volumeAvailable, 
// volumeDeleted, 
// volumeInUse, vpcAvailable, 
// vpcExists, 
// pnConnectionAvailable, 
// vpnConnectionDeleted, 
// vpcPeeringConnectionExists