"use strict";

const OWBase = require('./ops-works-base');
const AWS = require('aws-sdk');

const OW = class extends OWBase {

    constructor (options, data) {
        super(options);
        this._set(data);
    };

    _set (data) {
        if (data) {
            this.id = data.StackId;
            this.name = data.Name;
            this.data = data;
        }
    };

    _unset () {
        delete this.id;
        delete this.data;
    };
    
    create (options) {
        const params = {
            DefaultInstanceProfileArn: 'arn:aws:iam::517333335073:instance-profile/aws-opsworks-ec2-role', /* required */
            Name: options.name, /* required */
            Region: options.region, /* required */
            ServiceRoleArn: 'arn:aws:iam::517333335073:role/aws-opsworks-service-role', /* required */
            AgentVersion: 'LATEST',
            // Attributes: {
            //     someKey: 'STRING_VALUE',
            //     /* anotherKey: ... */
            // },
            // ChefConfiguration: {
            //     BerkshelfVersion: 'STRING_VALUE',
            //     ManageBerkshelf: true || false
            // },
            ConfigurationManager: {
                Name: 'Chef',
                Version: '12'
            },
            CustomCookbooksSource: {
                Type: options.cookbooksSource.type,
                Url: options.cookbooksSource.url
            },
         //   DefaultAvailabilityZone: 'STRING_VALUE',
         //   DefaultOs: 'STRING_VALUE',
         //   DefaultRootDeviceType: 'ebs | instance-store',
            DefaultSshKeyName: options.sshKey,
            DefaultSubnetId: options.subnetId,
            HostnameTheme: "Roman_Deities",
            UseCustomCookbooks: true,
            UseOpsworksSecurityGroups: false,
            VpcId: options.vpcId
        };
  
        const fn = (resolve, reject) => {
            this.service.createStack(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    this._set(data);
                    resolve(this);
                }       
            });
        };

        return new Promise(fn);
    };

    update (options) {
        const fn = (resolve, reject) => {
            resolve(this);
        };
         
        return new Promise(fn);
    };
    
    delete () {   
        const params = {
            StackId: this.id
        };

        const fn = (resolve, reject) => {
            this.service.deleteStack(params, (err, data) => {
                if (err) {
                    this.debug(err)
                    reject(err);
                } else {
                    this._unset();
                    this.debug(data)
                    resolve(data);
                }
            })
        };
        
        return new Promise(fn);
    };

    static GetAll (options) {
         return new Promise ((resolve, reject) => {
            const ow = new AWS.OpsWorks(options);
            ow.describeStacks({ }, function (err, data) {
                if (err) {
                    reject (err);
                } else {
                    resolve(data.Stacks.map(stack => new OW(options, stack)));
                }
            });
        });
    };

};

module.exports = OW;
