"use strict";

const LiftBase = require('../lift-base');

const OW = class extends LiftBase {
    constructor (options) {
        super(options, "OpsWorks");
    };
};

module.exports = OW;