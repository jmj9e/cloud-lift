"use strict";

const OWBase = require('./ops-works-base');
const AWS = require('aws-sdk');

const Layer = class extends OWBase {

    constructor (options, data) {
        super(options);
        this._set(data);
    };

    _set (data) {
         if (data) {
            this.id = data.LayerId;
            this.data = data;
            this.name = data.Name;
        }
    };

    _param (options) {
         return {
            Name: options.name, /* required */
            Shortname: options.shortName, /* required */
            StackId: options.stackId, /* required */
            Type: 'custom',
            // Attributes: {
            //     someKey: 'STRING_VALUE',
            //     /* anotherKey: ... */
            // },
            AutoAssignElasticIps: false,
            AutoAssignPublicIps: false,
       //     CustomInstanceProfileArn: 'STRING_VALUE',
      //      CustomJson: 'STRING_VALUE',
            // CustomRecipes: {
            //     Configure: [
            //     'STRING_VALUE',
            //     /* more items */
            //     ],
            //     Deploy: [
            //     'STRING_VALUE',
            //     /* more items */
            //     ],
            //     Setup: [
            //     'STRING_VALUE',
            //     /* more items */
            //     ],
            //     Shutdown: [
            //     'STRING_VALUE',
            //     /* more items */
            //     ],
            //     Undeploy: [
            //     'STRING_VALUE',
            //     /* more items */
            //     ]
            // },
            CustomSecurityGroupIds: options.securityGroupIds,
            // EnableAutoHealing: true ,
            // InstallUpdatesOnBoot: true,
            // LifecycleEventConfiguration: {
            //     Shutdown: {
            //         DelayUntilElbConnectionsDrained: true || false,
            //         ExecutionTimeout: 0
            //     }
            // },
            // Packages: [
            //     'STRING_VALUE',
            //     /* more items */
            // ],
            // UseEbsOptimizedInstances: true || false,
            // VolumeConfigurations: [
            //     {
            //         MountPoint: 'STRING_VALUE', /* required */
            //         NumberOfDisks: 0, /* required */
            //         Size: 0, /* required */
            //         Iops: 0,
            //         RaidLevel: 0,
            //         VolumeType: 'STRING_VALUE'
            //     },
            //     /* more items */
            // ]
        };
    };

    create (options) {
        const params = this._param(options);

        const fn = (resolve, reject) => { 
            this.service.createLayer(params, (err, data) => { 
                if (err) {
                    this.debug(err)
                    reject(err);
                } else {
                    data.Name = options.name;
                    this._set(data);
                    this.debug(data);
                    resolve(this)    
                }       
            });
        };

        return new Promise(fn);
    };

    update (options) {
        const params = this._param(options);
        params.LayerId = this.id;
        delete params.StackId;
        delete params.Type;

        const fn = (resolve, reject) => {
            this.service.updateLayer(params, (err, data) => {
                 if (err) {
                    this.debug(err);
                    reject(err);
                } else {
                    this.debug(data);
                    resolve(this)    
                } 
            });
        };

        return new Promise(fn);
    };

    attachLoadBalancer (name) {
        const params = {
            ElasticLoadBalancerName: name,
            LayerId: this.id
        };

         const fn = (resolve, reject) => {
            this.service.attachElasticLoadBalancer(params, (err, data) => {
                 if (err) {
                    this.debug(err);
                    reject(err);
                } else {
                    this.debug(data);
                    resolve(data);    
                } 
            });
        };

        return new Promise(fn);
    };

    getLoadBalancer () {
        const params = {
            LayerId: this.id
        };

        const fn = (resolve, reject) => {
            this.service.describeElasticLoadBalancers(params, (err, data) => {
                 if (err) {
                    this.debug(err);
                    reject(err);
                } else {
                    this.debug(data);
                    resolve(data);    
                } 
            });
        };

        return new Promise(fn);
    };

    static GetAll (options, stackId) {
        const params = { };
        if (stackId)  {
            params.StackId = stackId;
        }
        
        return new Promise ((resolve, reject) => {
            const ow = new AWS.OpsWorks(options);
            ow.describeLayers(params, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data.Layers.map(layer => new Layer(options, layer)));
                }
            });
        });
    };
};

module.exports = Layer;