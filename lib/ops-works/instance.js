"use strict";

const OWBase = require('./ops-works-base');
const AWS = require('aws-sdk');

const Instance = class extends OWBase {

    constructor (options, data) {
        super(options);
        this._set(data);
    };

    _set (data) {
         if (data) {
            this.id = data.LayerId;
            this.data = data;
            this.name = data.Name;
        }
    };

    create (options) {

        const params = {
            InstanceType: options.type,
            StackId: options.stackId,
            LayerIds: options.layerIds,
            AgentVersion: "INHERIT",
         //   AmiId: 'STRING_VALUE',
        //    Architecture: 'x86_64 | i386',
            // AutoScalingType: 'load | timer',
            // AvailabilityZone: 'STRING_VALUE',
            // BlockDeviceMappings: [
            //     {
            //     DeviceName: 'STRING_VALUE',
            //     Ebs: {
            //         DeleteOnTermination: true || false,
            //         Iops: 0,
            //         SnapshotId: 'STRING_VALUE',
            //         VolumeSize: 0,
            //         VolumeType: 'gp2 | io1 | standard'
            //     },
            //     NoDevice: 'STRING_VALUE',
            //     VirtualName: 'STRING_VALUE'
            //     },
            //     /* more items */
            // ],
         //   EbsOptimized: true || false,
            Hostname: options.name,
            // InstallUpdatesOnBoot: true || false,
            // Os: 'STRING_VALUE',
            // RootDeviceType: 'ebs | instance-store',
            SshKeyName: options.sshKey,
            SubnetId: options.subneyId,
            Tenancy: "default",
            // VirtualizationType: 'STRING_VALUE'
        };

        const fn = (resolve, reject) => {
            this.service.createInstance(params, (err, data) => {
                if (err) {
                    this.debug(err);
                    reject(err);
                } else {
                    this.debug(data);
                    this._set(data);
                    resolve(this);
                }
            });
        };

        return new Promise(fn);
    };

    update (options) {
        const fn = (resolve, reject) => {

        };
         
        return new Promise(fn);
    };

    delete () {
        const fn = (resolve, reject) => {

        };
        
        return new Promise(fn);
    };

    static GetAll (options) {
        const fn = (resolve, reject) => {

        };

        return new Promise(fn);
    };
};

module.exports = Instance;
