"use strict";

const LiftBase = require('../lift-base');

const IAM = class extends LiftBase {
    constructor (options) {
        super(options, "IAM");
    };

    tag (name, value) {
        const params = this.params;

        params.Resources = [this.id];
        params.Tags = params.Tags || [];
        params.Tags.push({ Key: name, Value: value });

        const fn = (resolve, reject) => {
            this.service.createTags(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        };

        return new Promise(fn);
    };
};

module.exports = IAM;