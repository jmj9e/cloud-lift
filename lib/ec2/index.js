"use strict";

module.exports = {
    VPC: require('./vpc'),
    IP: require('./ip'),
    Subnet: require('./subnet'),
    InternetGateway: require('./internet-gateway')
 //   NATGateway: require('./nat-gateway')
};
