"use strict";

const EC2Base = require('./ec2-base');
const AWS = require('aws-sdk');

const RouteTable = class extends EC2Base {

    constructor (options) {
        super(options);
    };

    create (vpc, options) {
        const fn = (resolve, reject) => {
            this.name = options.name;

            const params = this.params;
            params.VpcId = vpc.id;

            this.service.createRouteTable(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    this.data = data;
                    this.id = data.RouteTable.RouteTableId;

                    this.tag("Name", this.name)
                    .then(() => {
                        this.debug(data);
                        resolve(data);
                    }).catch((err) => {
                        this.debug(err);
                        reject(err);
                    });
                }
            });
        };

        return new Promise(fn);
    };

    addRoute (options) {
        const params = this.params;
        params.RouteTableId = this.id;

        if (options.type === "internet") {
            params.GatewayId = options.gatewayId;
        } else if (options.type === "nat") {
            params.NatGatewayId = options.gatewayId;
        }   

        this.service.createRoute(params, (err, data) => {

        });

    };

};

module.exports = RouteTable;
