"use strict";

const EC2Base = require('./ec2-base');
const AWS = require('aws-sdk');

const InternetGateway = class extends EC2Base {

    constructor (options, data) {
        super(options);

        if (data) {
            this.id = data.AllocationId;
            this.data = data;
        }
    };

    create (options) {
        this.name = options.name;

        const fn  = (resolve, reject) => {
            const params = this.params;
            this.service.createInternetGateway(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    this.id = data.InternetGateway.InternetGatewayId;
                    this.data = data;
                    this.tag("Name", this.name)
                    .then(() => {
                        return resolve(this)
                    })
                    .catch(err => reject(err));
                }   
            });
        };

        return new Promise(fn);
    };

    attach (vpc) {
        const fn  = (resolve, reject) => {
            const params = this.params;
            params.InternetGatewayId = this.id;
            params.VpcId = vpc.id;

            this.service.attachInternetGateway(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    this.attachmentId = data.AttachmentId;
                    resolve(this);
                }
            });
        };

        return new Promise(fn);
    };

    delete () {
        const params = this.params;
        params.InternetGatewayId = this.id;

        const fn = (resolve, reject) => {
            this.service.deleteInternetGateway(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        };

        return new Promise(fn);
    };

};

module.exports = InternetGateway;
