"use strict";

const EC2Base = require('./ec2-base');
const AWS = require('aws-sdk');

const VPC = class extends EC2Base {

    constructor (options, data) {
        super(options);
        this._set(data);
    };

    create (options) {
        this.name = options.name;

        // Params
        const params = this.params;
        params.CidrBlock = options.cidre;
        params.InstanceTenancy = options.tenency || "default" //'default | dedicated | host'

        const fn = (resolve, reject) => {
            this.service.createVpc(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    this._set(data); 
         
                    this.wait()
                    .then(res => this.tag("Name", this.name))
                    .then(data => resolve(this.data))
                    .catch(err => reject(err));     
                }       
            });
        };

        return new Promise(fn);
    };

    _set (data) {
        if (data) {
            this.id = data.VpcId;
            this.data = data;  
        }
    };

    wait () {
        const fn = (resolve, reject) => {
            this.service.waitFor('vpcAvailable', { VpcIds: [this.id] }, (err, data) => {
                if (err) {
                    this.log(err);
                    reject(err)
                } else {
                    this.log(data);
                    resolve(data);
                }
            });
        };

        return new Promise(fn);
    };

    delete () {
        const fn = (resolve, reject) => {
            this.service.deleteVpc({ VpcId: this.id }, (err, data) => {
                if (err) {
                    this.log(`failed to delete VPC id: ${vpc.VpcId}`);
                    reject(err);
                } else {
                    this.log(`Deleted VPC id: ${vpc.VpcId}`);
                    resolve(data);
                }
            });
        };

        return new Promise(fn);
    };

    static GetAll (options) {
        return new Promise ((resolve, reject) => {
            const ec2 = new AWS.EC2(options);
            ec2.describeVpcs({ }, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data.Vpcs.map(vpc => new VPC(options, vpc)));
                }
            });
        });
    };

    static DeleteAll (options) {
        return new Promise ((resolve, reject) => {
            VPC.GetAll(options).then(data => {
                const ec2 = new AWS.EC2(options);

                data.Vpcs.forEach(vpc => {
                    ec2.deleteVpc({ VpcId: vpc.VpcId }, (err, data) => {
                        if (err) {
                            this.log(`failed to delete VPC id: ${vpc.VpcId}`);
                        } else {
                            this.log(`Deleted VPC id: ${vpc.VpcId}`);
                        }
                    });
                });

                resolve(data);
            }).catch(err => {
                options.log(err)
                reject(err);
            });
        });
    };

};

module.exports = VPC;
