"use strict";

const EC2Base = require('./ec2-base');
const AWS = require('aws-sdk');

const Subnet = class extends EC2Base {

    constructor (options, data) {
        super(options);
        this._set(data);
    };

    _set (data) {
        if (data) {
            this.data = data;
            this.name = data.Name;
            this.vpcId = data.VpcId;
            this.id = data.SubnetId;
        }
    };

    /**
     * CReate new VPC
     * @param {*} vpc 
     * @param {*} options 
     */
    create (vpc, options) {
        this.name = options.name;

        const params = this.params;
        params.CidrBlock = options.cider; 
        params.VpcId = vpc.id;
        
        const fn = (resolve, reject) => {
            this.service.createSubnet(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    this._set(data);

                    this.wait()
                    .then(res => this.tag("Name", this.name))
                    .then(() => {
                        this.debug(data);
                        resolve(data);
                    })
                    .catch((err) => {
                        this.debug(err);
                        reject(err);
                    });
                }
            });
        };

        return new Promise(fn);
    };   

    wait () {
        const fn = (resolve, reject) => {
            const params = this.params;
            params.SubnetIds = [this.id];
            
            this.service.waitFor('subnetAvailable', params, (err, data) => {
                if (err) {
                    this.log(err);
                    reject(err)
                } else {
                    this.log(data);
                    resolve(data);
                }
            });
        };

        return new Promise(fn);
    };

    static GetAll (options) {
        const params = {
            Filters: [{
                Name: "vpc-id", 
                Values: options.vpcs
            }]
        };
        
        return new Promise ((resolve, reject) => {
            const ec2 = new AWS.EC2(options);
            ec2.describeSubnets(params, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data.Subnets.map(subnet => new Subnet(options, subnet)));
                }
            });
        });
    };

};

module.exports = Subnet;
