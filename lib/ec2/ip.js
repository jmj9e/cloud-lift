"use strict";

const EC2Base = require('./ec2-base');
const AWS = require('aws-sdk');

const IP = class extends EC2Base {

    constructor (options, data) {
        super(options);

        if (data) {
            this.id = data.AllocationId;
            this.data = data;
        }
    };

    create (options) {
        if (this.id) {
            return;
        }

        const params = {
            Domain: options.vpc ? 'vpc' : 'standard',
            DryRun: options.dryRun ? true : false
        };

        this.service.allocateAddress(params, function(err, data) {
            const fn = (resolved, reject) => {
                if (err) {
                    reject(err);
                } else {
                    this.id = data.AllocationId;
                    this.data = data;
                    resolved(data);
                }
            };

            return new Promise(fn);
        });
    };

    delete () {
        if (!this.id) {
            return
        }

        const params = this.parms;

        params.AllocationId = this.id;
    
        const fn = (resolved, reject) => {
            this.service.releaseAddress(params, function(err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolved(data);
                }
            });
        };

        return new Promise(fn);
    };

    static DeleteAll () {
        IP.GetAll().then(item => item.delete());
    };

    static GetAll (options) {
        const fn = (resolved, reject) => {
            const service = new AWS.EC2(options);
            service.describeAddresses({}, (err, data) => {
                if (err) {
                    console.log(err)
                    reject(err);
                } else {
                    const list = data.Addresses.map(address => new IP(options, address));
                    resolved(list);
                }
            });
        }

        return new Promise(fn);
    }
};

module.exports = IP;
