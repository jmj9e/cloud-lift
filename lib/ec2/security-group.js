"use strict";

const EC2Base = require('./ec2-base');
const AWS = require('aws-sdk');
const resourceName = "SG";

const SG = class extends EC2Base {

     constructor (options, data) {
        super(options);
        this._set(data);
    };

    _set (data) {
        if (data) {
            this.id = data.GroupId;
            this.vpcId = data.VpcId;
            this.name = data.GroupName;
            this.data = data;
        }
    };

    _unset() {
        delete this.id;
        delete this.data;
    }

    create (options) {
        console.log(options)
        const params = this.params;
        params.Description = options.description; /* required */
        params.GroupName = options.name; /* required */
        params.VpcId = options.vpcId;
  
        const fn = (resolve, reject) => {
            this.service.createSecurityGroup(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    this._set(data);
                    this.tag("Name", EC2Base.NameTag(resourceName, options.ENV, options.FUNC))
                    .then(data => options.ingress ? this.addIngressRules(options.ingress) : data)
                    .then(data => resolve(this))
                    .catch(err => reject(err));
                }
            });
        };

        return new Promise(fn);
    };

    update (options) {
        const fn = (resolve, reject) => {
            resolve(this);
        };

        return new Promise(fn);
    };

    addIngressRules (permissions) {
        const params = this.params;
        params.GroupId = this.id;

        params.IpPermissions = permissions.map(permission => {
            return {
                FromPort: permission.fromPort,
                IpProtocol: permission.ipProtocol || "tcp",
                IpRanges: [{ CidrIp: permission.cidrIp }],
                ToPort: permission.toPort
            };
        });

        const fn = (resolve, reject) => {
            this.service.authorizeSecurityGroupIngress(params, (err, data) => {
                err ? reject(err) : resolve(data);
            });
        };

        return new Promise(fn);
    };

    delete () {
        const params = this.params;
        params.GroupId = this.id;
     
        this.service.deleteSecurityGroup(params, (err, data) => {
            if (err) {
                this.log(err);
                reject(err)
            } else {
                this.log(data);
                resolve(data);
            }
        });
    
        return new Promise(fn);
    };

    static GetAll (options) {
        const params = { };

        if (options.groupNames) {
            params.Filters = [{ Name: "group-name", Values: options.groupNames }];
        }
        
        const fn = (resolve, reject) => {
            const ec2 = new AWS.EC2(options);
            ec2.describeSecurityGroups(params, function (err, groups) {
                if (err) {
                    reject(err);
                } else {
                    resolve(groups.SecurityGroups.map(sg => new SG(options, sg)));
                }
            });
        };

        return new Promise(fn);

    };

};

module.exports = SG;
